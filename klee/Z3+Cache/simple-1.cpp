#include <iostream>
#include <stdlib.h>
#include <sstream>

#include "z3++.h"


int main(){

    char const* spec = 
        "(declare-fun x () Int) (declare-fun y () Int) (assert (= 0 (+ y 1))\n)(assert (= (- x 1) y))";

    z3::context cxt;

    Z3_ast a = 
      Z3_parse_smtlib2_string(cxt.operator Z3_context(), 
                                spec,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0);

    z3::expr r = to_expr(cxt, a);

    z3::solver sol(cxt);

    sol.add(r);
    
    if(sol.check() == z3::sat){
      std::cout << "Saaaat:\n";
      std::cout << sol.get_model() << "\n";      
    }
    else
      printf("unsaaaat\n");

 }
