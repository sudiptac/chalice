//===-- MemoryManager.cpp -------------------------------------------------===//
//
//                     The KLEE Symbolic Virtual Machine
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "Common.h"

#include "CoreStats.h"
#include "Memory.h"
#include "MemoryManager.h"

#include "klee/ExecutionState.h"
#include "klee/Expr.h"
#include "klee/Solver.h"

#include "llvm/Support/CommandLine.h"

using namespace klee;

/***/

MemoryManager::~MemoryManager() { 
  while (!objects.empty()) {
    MemoryObject *mo = *objects.begin();
    if (!mo->isFixed)
      free((void *)mo->address);
    objects.erase(mo);
    delete mo;
  }
}

MemoryObject *MemoryManager::allocate(uint64_t size, bool isLocal, 
                                      bool isGlobal,
                                      const llvm::Value *allocSite,
																			unsigned xAddr,
																			int setSize, 
																			int blockSize) {

	/// sudiptac: support to make addresses in binary code and LLVM code 
  /// equivalent (in terms of memory performance) 
	if (xAddr) {	
		fprintf(stdout, "\n############ Address might be rewritten in KLEE @[%x] ############\n\n", xAddr);
		fflush(stdout);
	}

	/// sudiptac: setSize captures the number of sets in the cache
	/// This is an *unoptimized* hack (TODO: think better solutions)
	/// However, note that it is done only for global-pointer and stack-pointer
  if (size+setSize*blockSize > 10*1024*1024)
    klee_warning_once(0, "Large alloc: %u bytes.  KLEE may run out of memory.", (unsigned) size);

  uint64_t address = (uint64_t) (unsigned long) malloc((unsigned) (size + setSize*blockSize));
  if (!address)
    return 0;
  
  ++stats::allocations;

	/// sudiptac: manipulating addresses to keep equivalence with binary code
	int shift = 0;

	if (xAddr > 0)
	{
		int llvmSet = ((address + size - 1)/blockSize) % setSize;
		int simSet =  (xAddr/blockSize) % setSize;
		//int simSet =  0;
		shift = (llvmSet - simSet < 0) ? (simSet - llvmSet) : (simSet - llvmSet + setSize);
		int newllvmSet = ((address + size + blockSize * shift - 1) / blockSize) % setSize;
#ifdef _DEBUG
		fprintf(stdout, "LLVM address [%ld] mapped to cache set [%d]\n", (address + size - 1)/blockSize, llvmSet);
		fprintf(stdout, "Simulation address [0x%x] mapped to cache set [%d]\n", xAddr/blockSize, simSet);
		fprintf(stdout, "LLVM address [%lu] rewritten to [%lu]\n", address, address + blockSize * shift);
		fprintf(stdout, "Remapped cache set = [%d]\n", newllvmSet);
		fflush(stdout);
#endif
		assert((newllvmSet == simSet) && "address rewriting failed");
		//fprintf(stdout, "\n############ Rewriting address @[%x] ends ############\n", xAddr);
	}

  MemoryObject *res = new MemoryObject(address+blockSize*shift, size, isLocal, isGlobal, false,
                                       allocSite, this);
	res->shift = blockSize*shift;
  objects.insert(res);

  return res;
}

MemoryObject *MemoryManager::allocateFixed(uint64_t address, uint64_t size,
                                           const llvm::Value *allocSite) {
#ifndef NDEBUG
  for (objects_ty::iterator it = objects.begin(), ie = objects.end();
       it != ie; ++it) {
    MemoryObject *mo = *it;
    if (address+size > mo->address && address < mo->address+mo->size)
      klee_error("Trying to allocate an overlapping object");
  }
#endif

  ++stats::allocations;
  MemoryObject *res = new MemoryObject(address, size, false, true, true,
                                       allocSite, this);
  objects.insert(res);
  return res;
}

void MemoryManager::deallocate(const MemoryObject *mo) {
  assert(0);
}

void MemoryManager::markFreed(MemoryObject *mo) {
  if (objects.find(mo) != objects.end())
  {
		/// sudiptac: manipulating addresses to keep equivalence with binary code
    if (!mo->isFixed)
      free((void *)(mo->address - mo->shift));
    objects.erase(mo);
  }
}
